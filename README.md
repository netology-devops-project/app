# App

## Описание

Демо-приложение основное на официальном *Docker*-образе *Nginx*. Отдает статическую HTML-страницу с текстом *200 OK*.

## Сборка

```bash
docker build -t manokhin/netology-demo-app:version .
```

## Применение в кластере K8s

```bash
kubectl apply -f k8s/
```
